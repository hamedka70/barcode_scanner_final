package com.hamed.barcodescanner.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface PropertiesDao {

    @Query("SELECT * FROM properties")
    fun getAll(): LiveData<List<Properties>>

    @Query("SELECT * FROM properties ORDER BY propertiesID ASC ")
    fun getAllAsync(): List<Properties>

    /*@Query("SELECT * FROM BarcodeScanner where name LIKE  :Name AND barcode LIKE :Barcode")
    BarcodeScannerDataBase findByName(String Name, String Barcode);*/

    /*@Query("SELECT * FROM BarcodeScanner where name LIKE  :Name AND barcode LIKE :Barcode")
    BarcodeScannerDataBase findByName(String Name, String Barcode);*/
    @Query("SELECT COUNT(*) from properties")
    fun countProperties(): Int



    @Insert
    fun insert(properties: Properties)

    @Query("DELETE FROM properties WHERE propertiesID = :noteId")
    fun delete(noteId: Int): Int
}