package com.hamed.barcodescanner.data

import android.content.Context
import android.util.Log
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = arrayOf(Properties::class), version = 4, exportSchema = false)
abstract class PropertiesDatabase :RoomDatabase(){

    abstract fun propertiesDao():PropertiesDao

    companion object{
        @Volatile
        var INSTANCE: PropertiesDatabase?=null

        fun getPropertiesDatabase(context: Context): PropertiesDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            try {
                synchronized(this) {
                    var instance = Room.databaseBuilder(
                        context.applicationContext,
                        PropertiesDatabase::class.java,
                        "barcode_scanner"
                    ).build()
                    INSTANCE = instance
                    return instance
                }
            } catch (e: Exception) {
                Log.e("ert", checkNotNull(e.message))
            }
            return INSTANCE!!
        }
       fun destroyDatabase(){INSTANCE }
    }
}