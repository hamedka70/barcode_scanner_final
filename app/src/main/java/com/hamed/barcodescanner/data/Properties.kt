package com.hamed.barcodescanner.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.intellij.lang.annotations.Identifier


@Entity(tableName = "properties")
data class Properties(
                      @ColumnInfo(name = "image_address")  var imageAddress: String?
                      ,@ColumnInfo(name = "properties_name") var propertiesName: String?
                      ,@ColumnInfo(name = "barcode") var barcode: String
                      ,@ColumnInfo(name = "time") var time:String
                      ,@ColumnInfo(name = "barcode_format") var barcodeFormat:String?) {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "propertiesID")
    var id=0


        var tickChecked=false


}