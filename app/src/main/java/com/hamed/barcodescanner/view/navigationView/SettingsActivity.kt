package com.hamed.barcodescanner.view.navigationView

import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.getSystemService
import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SwitchPreferenceCompat
import com.hamed.barcodescanner.R
import com.hamed.barcodescanner.utilities.lang
import com.hamed.barcodescanner.view.ScannerActivity


class SettingsActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.settings, SettingsFragment())
            .commit()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }


    class SettingsFragment : PreferenceFragmentCompat() {
        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            try {
                setPreferencesFromResource(R.xml.root_preferences, rootKey)

            }catch (e:Exception){
                Log.e("er",checkNotNull(e.message))
            }
            val languageListPreference = findPreference<ListPreference>("LANGUAGE_KEY")
            languageListPreference!!.onPreferenceChangeListener =
                Preference.OnPreferenceChangeListener { preference, newValue ->
                    if (newValue == "farsi") {
                        setNewLocale(requireContext(), lang.FARSI)
                        languageListPreference!!.value = "farsi"
                    }
                    if (newValue == "english") {
                        setNewLocale(requireContext(), lang.ENGLISH)
                        languageListPreference!!.value = "english"
                    }
                    false
                }

            val soundPreference=findPreference<ListPreference>("SOUND_KEY")
            var amanager= requireContext().getSystemService(Context.AUDIO_SERVICE) as AudioManager
            soundPreference!!.onPreferenceChangeListener =
                Preference.OnPreferenceChangeListener { preference, newValue ->
                    if (newValue == "on") {
                        amanager.setStreamMute(AudioManager.STREAM_MUSIC, false);
                        soundPreference.value = "on"
                    }
                    if (newValue == "off") {
                        amanager.setStreamMute(AudioManager.STREAM_MUSIC, true);
                        soundPreference.value = "off"
                    }
                    false
                }


        }
        fun setNewLocale(mContext: Context?, @lang.LocaleDef language: String?) {
            lang.setNewLocale(mContext, language)
            val intent = requireActivity()!!.intent
            startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this, ScannerActivity::class.java)
        startActivity(intent)
    }
}