package com.hamed.barcodescanner.view

import android.app.Dialog
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.util.Log
import android.view.*
import android.widget.Filter
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.recyclerview.widget.RecyclerView
import com.hamed.barcodescanner.R
import com.hamed.barcodescanner.adapters.*
import com.hamed.barcodescanner.data.Properties
import com.hamed.barcodescanner.databinding.DeleteDialogBinding
import com.hamed.barcodescanner.databinding.FragmentHistoryBinding
import com.hamed.barcodescanner.utilities.deleteImageSdCard
import com.hamed.barcodescanner.utilities.vibrate
import com.hamed.barcodescanner.viewmodels.HistoryViewModel
import kotlinx.android.synthetic.main.delete_dialog.*


class HistoryFragment : Fragment() {
    private lateinit var mView: View
    lateinit var binding: FragmentHistoryBinding
    var mEditMenuVisible = false
    lateinit var searchView: SearchView
    var mSearchSelected = -1
    var mSearchPropertiesList = ArrayList<Properties>()
    var mAllPropertiesList = ArrayList<Properties>()
    var historyAdapter = HistoryAdapter(this)
    lateinit var viewModel: HistoryViewModel


    companion object {
        fun newInstance() =
            HistoryFragment()

    }

    override fun onStart() {
        super.onStart()
        try {
        } catch (e: java.lang.Exception) {
            Log.e("err", checkNotNull(e.message))
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_historys, container, false)
        viewModel = ViewModelProvider(this).get(HistoryViewModel::class.java)

        binding.properties = viewModel
        //binding.barcodescannerRecyclerview.adapter = historyAdapter
        viewModel.mAllProperties.observe(viewLifecycleOwner) { array ->
            viewModel.Properties.value = array
            mAllPropertiesList = array as ArrayList<Properties>

        }
        viewModel.Properties.observe(viewLifecycleOwner) { list ->

            subscribeRecyclerView(binding.barcodescannerRecyclerview)
            historyAdapter.setPropetiesViewModelList(list as ArrayList<Properties>)
            getVisibleEmptyIV(list)
        }
        setHasOptionsMenu(true)
        return binding.root
    }

    private fun subscribeRecyclerView(recyclerView: RecyclerView) {
        //historyAdapter.setHasStableIds(true);
        recyclerView.adapter = historyAdapter
        recyclerView.setHasFixedSize(true)

    }

    private fun getVisibleEmptyIV(list: List<Properties>) {
        if (list.isNotEmpty()) {
            binding.historyRecycleEmptyImageView.visibility = View.GONE
            binding.historyRecycleEmptyTextView.visibility = View.GONE
        } else {
            binding.historyRecycleEmptyImageView.visibility = View.VISIBLE
            binding.historyRecycleEmptyTextView.visibility = View.VISIBLE

        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        try {
            inflater.inflate(R.menu.history_menu, menu)
            if (mEditMenuVisible) {
                menu.findItem(R.id.action_search).isVisible = false
                menu.findItem(R.id.action_delete).isVisible = true
                menu.findItem(R.id.action_selectAll).isVisible = true
            } else {
                menu.findItem(R.id.action_search).isVisible = true
                menu.findItem(R.id.action_delete).isVisible = false
                menu.findItem(R.id.action_selectAll).isVisible = false
            }
            val searchManager =
                requireActivity().getSystemService(Context.SEARCH_SERVICE) as SearchManager
            searchView = menu.findItem(R.id.action_search)
                .actionView as SearchView
            searchView.setSearchableInfo(
                searchManager
                    .getSearchableInfo(requireActivity().componentName)
            )
            searchView.setMaxWidth(Int.MAX_VALUE)
            searchView.setOnQueryTextListener(object :
                SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String): Boolean {
                    //mBarcodePendingAdapter.getFilter().filter(query);
                    //mAdabter.getFilter().filter(query);
                    return true
                }

                override fun onQueryTextChange(newText: String): Boolean {
                    getFilter()?.filter(newText)
                    //getActivity().invalidateOptionsMenu();
                    return true
                }
            })
        } catch (e: Exception) {
            Log.e("Error", checkNotNull(e.message))
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_search -> {
            }
            R.id.action_delete -> {
                loadDeleteDialog()
                requireContext().vibrate(30)
            }
            R.id.action_selectAll -> {
                selectAll()
                requireContext().vibrate(30)
            }
        }
        return false
    }


    fun getFilter(): Filter? {
        mSearchSelected = 1
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    mSearchPropertiesList = mAllPropertiesList
                } else {
                    val filteredList: ArrayList<Properties> = ArrayList<Properties>()
                    for (row in viewModel.Properties.value as ArrayList<Properties>) {
                        if (row.propertiesName == null) {
                            row.propertiesName = " "
                        }
                        if (row.propertiesName!!.contains(charSequence) || row.barcode.contains(
                                charSequence
                            )
                        ) {
                            filteredList.add(row)
                        }
                    }
                    mSearchPropertiesList = filteredList
                }
                val filterResults = FilterResults()
                filterResults.values = mSearchPropertiesList
                return filterResults
            }

            override fun publishResults(
                charSequence: CharSequence,
                filterResults: FilterResults
            ) {
                mSearchPropertiesList = filterResults.values as ArrayList<Properties>
                viewModel.Properties.value = mSearchPropertiesList
            }
        }
    }

    private fun loadDeleteDialog() {
        val dialogBinding: DeleteDialogBinding = DataBindingUtil.inflate(
            LayoutInflater.from(requireContext()),
            R.layout.delete_dialog,
            null,
            false
        )
        val dialog = Dialog(requireContext())
        dialog.setContentView(dialogBinding.root)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window?.attributes?.windowAnimations = R.style.DialogAnimation
        dialog.show()
        dialog.setCancelable(false)
        dialog.deletdialog_yes_button.setOnClickListener(View.OnClickListener {
            viewModel.deleteItem()
            dialog.cancel()
            toggleDeleteToolbar(true)
            requireContext().vibrate(30)
        })
        dialog.deletdialog_no_button.setOnClickListener(View.OnClickListener {
            dialog.cancel()
            unSelectAll()
            toggleDeleteToolbar(true)
            requireContext().vibrate(30)
        })
    }

    private fun selectAll() {
        for (item in mAllPropertiesList) {
            if (!item.tickChecked) {
                item.tickChecked = true
            }
        }
        viewModel.Properties.value = mAllPropertiesList
    }

    private fun unSelectAll() {
        for (item in mAllPropertiesList) {
            if (item.tickChecked) {
                item.tickChecked = false
            }
        }
        viewModel.Properties.value = mAllPropertiesList
    }


    fun toggleDeleteToolbar(emptySelected: Boolean) {
        if (emptySelected) {
            mEditMenuVisible = false
            activity?.invalidateOptionsMenu()
        } else {
            mEditMenuVisible = true
            activity?.invalidateOptionsMenu()
        }
    }


}