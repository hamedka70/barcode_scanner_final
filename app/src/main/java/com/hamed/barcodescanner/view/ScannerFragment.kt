package com.hamed.barcodescanner.view

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.zxing.BarcodeFormat
import com.google.zxing.client.android.BeepManager
import com.hamed.barcodescanner.R
import com.hamed.barcodescanner.data.Properties
import com.hamed.barcodescanner.databinding.FragmentScannerBinding
import com.hamed.barcodescanner.utilities.*
import com.hamed.barcodescanner.viewmodels.ScannerViewModel
import com.journeyapps.barcodescanner.*
import com.journeyapps.barcodescanner.camera.CameraSettings

import java.util.*



class ScannerFragment : Fragment() {


    private lateinit var mBeepManager: BeepManager
    private lateinit var mLastText: String
    private lateinit var mProperties: Properties
    var mImageAddress: String?=null
    private lateinit var mView: View
    private var mFlashLightState = true
    private var mCameraState = true
    private val solarDate:ShamsiDate = ShamsiDate()
    lateinit var captureManager: CaptureManager
    private lateinit var mBarcodeView:DecoratedBarcodeView
    lateinit var scannerViewModel: ScannerViewModel

    companion object {
        fun newInstance(): Fragment {
            return ScannerFragment()
        }
    }



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val bindingFragmentScanner= FragmentScannerBinding.inflate(inflater,container,false)
        mBarcodeView = bindingFragmentScanner.barcodescannerDecorate
        scannerViewModel = ViewModelProvider(this).get(ScannerViewModel::class.java)

        val formats: Collection<BarcodeFormat> = Arrays.asList(BarcodeFormat.QR_CODE, BarcodeFormat.CODE_39)
        mBarcodeView.decoderFactory=DefaultDecoderFactory(formats)
        mBarcodeView.initializeFromIntent(requireActivity().intent)
        mBarcodeView.decodeContinuous(callback)
        mBarcodeView.setStatusText("")
        mBeepManager = BeepManager(activity)

        mLastText=""
        setHasOptionsMenu(true)
        mView = bindingFragmentScanner.root
        return mView
    }




    private val callback: BarcodeCallback = object : BarcodeCallback {
        override fun barcodeResult(result: BarcodeResult) {
            if (result.text == mLastText) {
                // Prevent duplicate scans

                // Prevent duplicate scans
                return
            }

            mLastText = result.text
            mBarcodeView.setStatusText("")
            val format: String = result.barcodeFormat.toString()
            mBeepManager.playBeepSoundAndVibrate()
            mBarcodeView.pause()
            try {
                val NewDocumentDialog = Dialog(activity!!)
                NewDocumentDialog.setContentView(R.layout.barcodescanner_dialog_enter_tag)
                NewDocumentDialog.setCancelable(true)
                onPause()
                val wareName =  NewDocumentDialog.findViewById<EditText>(R.id.generate_barcode_editText)
                val save =  NewDocumentDialog.findViewById<Button>(R.id.barcodescanner_warename_savebutton)
                val cancel = NewDocumentDialog.findViewById<Button>(R.id.barcodescanner_warename_cancelbutton)
                val resultImageView =   NewDocumentDialog.findViewById<ImageView>(R.id.barcode_scanner_imageview)
               generateImage(
                    resultImageView,
                    result.getBitmapWithResultPoints(Color.RED)
                )
                NewDocumentDialog.setCancelable(false)
                NewDocumentDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                NewDocumentDialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
                NewDocumentDialog.show()
                save.setOnClickListener {

                    if (ScannerActivity.CHECKPERMISSION(ScannerActivity.REQUESTWRITESTORAGE,requireContext())) {
                        try {
                            // NewDocumentDialog.context.vibration(35)
                            val product = wareName.text.toString()
                            val currentDate: String = ShamsiDate.getCurrentShamsidate()
                            val cc = Calendar.getInstance()
                            val currentTime = String.format(
                                "%02d:%02d:%02d"
                                , cc[Calendar.HOUR_OF_DAY]
                                , cc[Calendar.MINUTE], cc[Calendar.SECOND]
                            )
                            val imgName = currentDate.replace("/", "") + currentTime
                            mImageAddress = saveImageSDCARD(   result.getBitmapWithResultPoints(Color.RED),   imgName)

                            var time=
                                """
                            $currentDate
                            $currentTime
                            """.trimIndent()


                            mProperties=Properties(mImageAddress,product,mLastText,time,format)
                            scannerViewModel.insert(mProperties)
                            val toast =
                                Toast.makeText(NewDocumentDialog.context, getString(R.string.save_barcode_alert), Toast.LENGTH_SHORT)
                            toast.show()
                            NewDocumentDialog.cancel()
                            onResume()
                        } catch (e: Exception) {
                            Log.e("Error",checkNotNull(e.message))
                        }
                    }else{
                        requireActivity().toast(getString(R.string.write_storage_permission_alert))
                        ScannerActivity.REQUESTPERMISSION(ScannerActivity.REQUESTWRITESTORAGE,ScannerActivity.WRITESTORAGE,requireActivity())
                    }

                }
                cancel.setOnClickListener {
                    //NewDocumentDialog.context.vibration(35)
                    NewDocumentDialog.cancel()
                    onResume()
                }
            } catch (e: java.lang.Exception) {
                Log.e("Error", checkNotNull(e.message))
            }

        }

    }


     override fun onResume() {
        super.onResume()
        mBarcodeView.resume()
    }

     override fun onPause() {
        super.onPause()
        mBarcodeView.pause()
    }

    fun pause(view: View?) {
        mBarcodeView.pause()
    }

    fun resume(view: View?) {
        mBarcodeView.resume()
    }

    fun triggerScan(view: View?) {
        mBarcodeView.decodeSingle(callback)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.scanner_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_flash -> {
                flashLightButton()
                requireContext().vibrate(30)
            }
            R.id.action_selfiCamera ->{
                frontCameraButton()
                requireContext().vibrate(30)
            }
        }
        return false
    }

    fun flashLightButton() {
        mFlashLightState = if (mFlashLightState) {
            mBarcodeView.setTorchOn()
            false
        } else {
            mBarcodeView.setTorchOff()
            true
        }
    }

    fun frontCameraButton() {
        if (mCameraState) {
            val cameraSettings = CameraSettings()
            cameraSettings.requestedCameraId = 1
            mBarcodeView.cameraSettings = cameraSettings
            mBarcodeView.pause()
            mBarcodeView.resume()
            mCameraState = false
        } else {
            val cameraSettings = CameraSettings()
            cameraSettings.requestedCameraId = 0
            mBarcodeView.cameraSettings = cameraSettings
            mBarcodeView.pause()
            mBarcodeView.resume()
            mCameraState = true
        }
    }

}