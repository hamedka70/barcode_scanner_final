package com.hamed.barcodescanner.view

import android.Manifest
import android.app.Activity
import android.app.Application
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import com.google.firebase.messaging.FirebaseMessaging
import com.hamed.barcodescanner.R
import com.hamed.barcodescanner.databinding.ActivityMainBinding
import com.hamed.barcodescanner.services.MyFirebaseMessagingService
import com.hamed.barcodescanner.utilities.initSettings
import com.hamed.barcodescanner.utilities.vibrate
import com.hamed.barcodescanner.view.navigationView.AboutUs
import com.hamed.barcodescanner.view.navigationView.SettingsActivity
import com.hamed.barcodescanner.view.navigationView.dialogHelp
import com.hamed.barcodescanner.viewmodels.ScannerActivityViewModel
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.activity_scanner.view.*

class ScannerActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var binding: ActivityMainBinding
    lateinit var mButtonNavigationView: BottomNavigationView
    lateinit var viewModel: ScannerActivityViewModel


    companion object{
        val CAMERA= 0x1
        val READSTORAGE = 0x2
        val WRITESTORAGE= 0x3
        val REQUESTCAMERA = Manifest.permission.CAMERA
        val REQUESTREADSTORAGE = Manifest.permission.READ_EXTERNAL_STORAGE
        val REQUESTWRITESTORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE

         fun CHECKPERMISSION(permission:String,context: Context): Boolean {
            val result = ContextCompat.checkSelfPermission(context, permission)
            return if (result == PackageManager.PERMISSION_GRANTED) {
                true
            } else {
                false
            }
        }
         fun REQUESTPERMISSION(permission:String,permissionCode:Int,activity:Activity) {
            ActivityCompat.requestPermissions(activity, arrayOf(permission), permissionCode
            )
        }

    }




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewModel= ViewModelProvider(this).get(ScannerActivityViewModel::class.java)


        binding.viewmodel=viewModel
        startService(Intent(applicationContext,MyFirebaseMessagingService::class.java))
        initSettings(this)
        setSupportActionBar(binding.drawerLayout.include1.activity_barcode_scanner_toolbar)
        val toggle = ActionBarDrawerToggle(this,binding.drawerLayout, binding.drawerLayout.include1.activity_barcode_scanner_toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        binding.drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        binding.navView.setNavigationItemSelectedListener(this)
        mButtonNavigationView = binding.drawerLayout.include1.activity_barcode_scanner_bottom_nav
        //viewModel.setOnNavigationItemSelected(mButtonNavigationView)
        mButtonNavigationView.setOnNavigationItemSelectedListener(navigationItemSelectedListener)
        if(supportFragmentManager.backStackEntryCount==0){
            openFragment(ScannerFragment.newInstance())
        }
        REQUESTPERMISSION(REQUESTCAMERA, CAMERA,this)
    }

    var navigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_home -> {
                    REQUESTPERMISSION(REQUESTCAMERA, CAMERA,this)
                    REQUESTPERMISSION(REQUESTWRITESTORAGE, WRITESTORAGE,this)
                    openFragment(ScannerFragment.newInstance())
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_list -> {
                    REQUESTPERMISSION(REQUESTREADSTORAGE, READSTORAGE,this)
                    openFragment(HistoryFragment.newInstance())
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_add -> {
                    REQUESTPERMISSION(REQUESTWRITESTORAGE, WRITESTORAGE,this)
                    openFragment(GenerateFragment.newInstance())
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }


    fun openFragment(fragment: Fragment) {
            val transaction =   supportFragmentManager.beginTransaction()
            transaction.replace(R.id.fl_framelayout, fragment)
            transaction.addToBackStack(null)
            transaction.commit()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.nav_settings ->{
                val intent=Intent(this,SettingsActivity::class.java)
                startActivity(intent)
                binding.navView.foregroundGravity=GravityCompat.START
            }
            R.id.nav_about_us -> {
                val intent=Intent(this,AboutUs::class.java)
                startActivity(intent)
                binding.navView.foregroundGravity=GravityCompat.START
            }
            R.id.nav_help ->{
                binding.navView.foregroundGravity=GravityCompat.START
                dialogHelp(this)
            }
        }
        return true
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 1) {
            exitDialog()
        } else {
            super.onBackPressed()
        }
    }

    fun exitDialog() {
        try {
            val exitDialog = Dialog(this)
            exitDialog.setContentView(R.layout.close_app_dialog)
            val yesButton =
                exitDialog.findViewById<Button>(R.id.exitdialog_yes_button)
            val noButton =
                exitDialog.findViewById<Button>(R.id.exitdialog_no_button)
            exitDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            exitDialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
            exitDialog.show()
            exitDialog.setCancelable(true)
            yesButton.setOnClickListener {
                finishAffinity()
                exitDialog.cancel()
                this.vibrate(30)

            }
            noButton.setOnClickListener {
                exitDialog.cancel()
                this.vibrate(30)
            }
        } catch (er: java.lang.Exception) {
            Log.e("Error",checkNotNull(er.message))
        }
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode){
            CAMERA  -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.e("value", "Permission Granted, Now you can use camera .")
            } else {
                Log.e("value", "Permission Denied, You cannot use camera .")
            }
            READSTORAGE  -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.e("value", "Permission Granted, Now you can use read .")
            } else {
                Log.e("value", "Permission Denied, You cannot use read .")
            }

            WRITESTORAGE  -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.e("value", "Permission Granted, Now you can use write .")
            } else {
                Log.e("value", "Permission Denied, You cannot use write .")
            }
        }
    }


}