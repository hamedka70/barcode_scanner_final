package com.hamed.barcodescanner.view.navigationView

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import com.hamed.barcodescanner.R

fun dialogHelp(context: Context){
    val helpDialog = Dialog(context)
    helpDialog.setContentView(R.layout.help_dialog)
    helpDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    helpDialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
    helpDialog.show()
    helpDialog.setCancelable(true)
}