package com.hamed.barcodescanner.view.navigationView

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.hamed.barcodescanner.R
import com.hamed.barcodescanner.view.ScannerActivity

class AboutUs : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about_us)
        setSupportActionBar(findViewById(R.id.toolbar))
        findViewById<CollapsingToolbarLayout>(R.id.toolbar_layout).title = getString(R.string.menu_about_us)
        findViewById<FloatingActionButton>(R.id.fab).setOnClickListener { view ->
            val emailIntent = Intent(
                Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", "hamedzrmb@gmail.com", null
                )
            )
/*            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject")
            emailIntent.putExtra(Intent.EXTRA_TEXT, "Body")*/
            startActivity(Intent.createChooser(emailIntent, "Send email..."))
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this, ScannerActivity::class.java)
        startActivity(intent)
    }
}