package com.hamed.barcodescanner.view

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.HandlerCompat.postDelayed
import androidx.databinding.DataBindingUtil
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import com.hamed.barcodescanner.R
import com.hamed.barcodescanner.databinding.ActivitySplashBinding
import com.hamed.barcodescanner.utilities.initSettings
import java.io.File

class SplashActivity : AppCompatActivity() {

    lateinit var binding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this , R.layout.activity_splash)

        if(Build.VERSION.SDK_INT >22 )
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

        Handler().postDelayed({
            startActivity(Intent(applicationContext, ScannerActivity::class.java))
            finish()
        }, 2000)

        initSettings(this)
        makeAppFolder()


    }

    private fun makeAppFolder(){
        try{
            val sdCardRoot = Environment.getExternalStorageDirectory().toString()
            val myDir = File("$sdCardRoot/Pictures/Iranian BS")

            var success = true
            if (!myDir.exists()) {
                success = myDir.mkdirs()
            }
            if (success) {
                // Do something on success
            } else {
                // Do something else on failure
            }
        }catch (e:Exception){
            Log.e("folder app ", checkNotNull(e.message))
        }
    }
}