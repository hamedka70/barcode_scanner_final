package com.hamed.barcodescanner.view

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.zxing.MultiFormatWriter
import com.hamed.barcodescanner.R
import com.hamed.barcodescanner.databinding.FragmentGenerateBinding
import com.hamed.barcodescanner.databinding.GenerateDialogBinding
import com.hamed.barcodescanner.utilities.bitmap
import com.hamed.barcodescanner.utilities.getBarcodeFormat
import com.hamed.barcodescanner.utilities.toast
import com.hamed.barcodescanner.utilities.vibrate
import com.hamed.barcodescanner.viewmodels.GenerateViewModel


class GenerateFragment : Fragment() {

    private lateinit var mView: View
    lateinit var viewModel: GenerateViewModel
    var mBarcodeFormat: String = ""
    val multiFormatWriter = MultiFormatWriter()
    lateinit var binding: FragmentGenerateBinding
    var mContext:Context?=null
    companion object {
        fun newInstance() = GenerateFragment()

    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_generate, container, false)
        mView = binding.root
        viewModel = ViewModelProvider(this).get(GenerateViewModel::class.java)
        binding.onClick=object :
            Callback {
            override fun Insert() {
                if (ScannerActivity.CHECKPERMISSION(ScannerActivity.REQUESTWRITESTORAGE,requireContext())) {
                    if (binding.genereteTextinputEdittext.text!!.isNotEmpty()) {
                        viewModel.insert()
                        loadGenerateAlertDialog()
                    } else
                        requireActivity().toast("Please Enter Barcode Value!!!")
                }else{
                    requireActivity().toast(getString(R.string.write_storage_permission_alert))
                    ScannerActivity.REQUESTPERMISSION(ScannerActivity.REQUESTWRITESTORAGE,ScannerActivity.WRITESTORAGE,requireActivity())
                }
                requireContext().vibrate(30)
            }

        }
        binding.item = viewModel
        viewModel.mBarcodeFormatLiveData.observe(viewLifecycleOwner, Observer<String> { bf: String ->
            viewModel.mBarcodeFormat = bf
            mBarcodeFormat = bf
            //binding.generateBarcodeEditText.editText.text.clear()
            binding.generateBarcodeImageView.setImageBitmap(null)
            binding.genereteTextinputEdittext.text?.clear()
        })

        viewModel.mBarcodeLiveData.observe(viewLifecycleOwner, Observer<String> { br: String ->
            viewModel.bitmap.value=bitmap(br, getBarcodeFormat(mBarcodeFormat), multiFormatWriter)
            viewModel.mBarcode=br
            binding.fragmentGenetrateTv.text=br
            if(br !=""){
                binding.generateBarcodeButton.isEnabled=true
                binding.generateBarcodeNameEditText.isEnabled=true
            }else{
                binding.generateBarcodeButton.isEnabled=false
                binding.generateBarcodeNameEditText.isEnabled=false
                binding.generateBarcodeImageView.setImageBitmap(null)
            }
        })

        return mView

    }

    fun loadGenerateAlertDialog(){
        val dialogBinding: GenerateDialogBinding
                = DataBindingUtil.inflate(
            LayoutInflater.from(requireContext()),
            R.layout.generate_dialog,null,false)
        val dialog= Dialog(requireContext())
        dialog.setContentView(dialogBinding.root)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window?.attributes?.windowAnimations= R.style.DialogAnimation
        dialog.show()
        dialog.setCancelable(false)
        dialogBinding.generatedialogOkButton.setOnClickListener(View.OnClickListener {
            binding.genereteTextinputEdittext.text?.clear()
            binding.generateBarcodeNameEditText.text?.clear()
            binding.generateBarcodeImageView.setImageBitmap(null)
            dialog.cancel()
        })
    }




    interface Callback{
        fun Insert()
    }

}