package com.hamed.barcodescanner.Repository

import android.util.Log
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.hamed.barcodescanner.data.Properties
import com.hamed.barcodescanner.data.PropertiesDao

class PropertiesRepository(private val propertiesDao: PropertiesDao) {

    var Properties =MutableLiveData<List<Properties>>()
    var allProperties= propertiesDao.getAll()
    suspend fun  insert(properties: Properties) =  propertiesDao.insert(properties)
    suspend fun delete(id:Int)= propertiesDao.delete(id)
    suspend fun countProperties():Int = propertiesDao.countProperties()


init {
    Properties.value
}

}