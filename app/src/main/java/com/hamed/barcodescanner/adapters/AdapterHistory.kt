package com.hamed.barcodescanner.adapters

import android.app.Dialog
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.core.content.ContextCompat.startActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.hamed.barcodescanner.R
import com.hamed.barcodescanner.data.Properties
import com.hamed.barcodescanner.databinding.ItemPropertiesBinding
import com.hamed.barcodescanner.utilities.getImage
import com.hamed.barcodescanner.utilities.vibrate
import com.hamed.barcodescanner.view.HistoryFragment
import com.hamed.barcodescanner.viewmodels.HistoryAdapterViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.net.HttpCookie.parse
import java.net.URL
import java.util.*
import java.util.logging.Level.parse
import kotlin.collections.ArrayList


lateinit var mContext: Context

class HistoryAdapter(historyFragment: HistoryFragment) : RecyclerView.Adapter<HistoryAdapter.CustomHolder>() {

    var mPropertiesViewModelList = ArrayList<Properties>()
    var mAllProperties = ArrayList<Properties>()
    lateinit var binding:ItemPropertiesBinding
    var mHistoryFragment: HistoryFragment

    companion object {
        var ISEDITABLE = true
        var EMPITYSELECTED = true
    }

    init {
        mHistoryFragment=historyFragment

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomHolder {
        mContext = parent.context

        val mLayoutInflater = LayoutInflater.from(mContext)
        binding = DataBindingUtil.inflate(mLayoutInflater, R.layout.rv_item_history, parent, false)
        return CustomHolder(binding)
    }

    override fun getItemCount(): Int {
        return mPropertiesViewModelList.size
    }

    override fun onBindViewHolder(holder: CustomHolder, position: Int) {

        val item = mPropertiesViewModelList[position]
        holder.bind(item)

    }



    fun setPropetiesViewModelList(propetiesViewModelList: ArrayList<Properties>) {
        mPropertiesViewModelList = propetiesViewModelList
        Collections.reverse(mPropertiesViewModelList)
        mAllProperties = propetiesViewModelList
        //notifyDataSetChanged()
    }

    inner class CustomHolder(private val itemBinding: ItemPropertiesBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {


        fun bind(propertiesModel: Properties) {
            itemBinding.viewmodel =
                HistoryAdapterViewModel(
                    propertiesModel
                )
            itemBinding.onClick = HistoryItemClick()
            itemBinding.executePendingBindings()
            try {
                itemBinding.barcodescannerImg.setImageBitmap(getImage(propertiesModel))

            }catch (e:Exception){
                Log.e("e",checkNotNull(e.message))
            }
            //itemBinding.barcodescannerImg.borderColor= randomColor()


            itemBinding.constraintLayout.setOnLongClickListener(View.OnLongClickListener {
                mContext.vibrate(30)
                    propertiesModel.tickChecked=!propertiesModel.tickChecked
                    bindIsGone(itemBinding.rvImageChecked,propertiesModel.tickChecked)
                    for (item in mPropertiesViewModelList){
                        if (item.tickChecked){
                            EMPITYSELECTED=false
                            break
                        }else{
                            EMPITYSELECTED=true
                        }
                    }

                for (item in mPropertiesViewModelList){
                    if (item.tickChecked){
                        mHistoryFragment.toggleDeleteToolbar(!ISEDITABLE)
                        break
                    }else{
                        mHistoryFragment.toggleDeleteToolbar(ISEDITABLE)

                    }
                }
                mHistoryFragment.toggleDeleteToolbar(EMPITYSELECTED)
                    return@OnLongClickListener true
            })

            itemBinding.constraintLayout.setOnClickListener(View.OnClickListener {
                mContext.vibrate(30)
                for (item in mPropertiesViewModelList){
                    if (item.tickChecked){
                        EMPITYSELECTED=false
                        break
                    }else{
                        EMPITYSELECTED=true
                    }
                }
                if (!EMPITYSELECTED){
                    propertiesModel.tickChecked=!propertiesModel.tickChecked
                    bindIsGone(itemBinding.rvImageChecked,propertiesModel.tickChecked)
                }
                for (item in mPropertiesViewModelList){
                    if (item.tickChecked){
                        mHistoryFragment.toggleDeleteToolbar(!ISEDITABLE)
                        break
                    }else{
                        mHistoryFragment.toggleDeleteToolbar(ISEDITABLE)
                    }
                }

            })

        itemBinding.barcodescannerImg.setOnClickListener(View.OnClickListener {
            mContext.vibrate(30)
            openImageDialog(propertiesModel)

/*            try {
    mContext.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(propertiesModel.imageAddress)))
}catch (e:Exception){
    Log.e("er",e.message)
}*/


        })

        }
    }


    fun getSelected(): ArrayList<Properties>? {
        val selected = ArrayList<Properties>()
        for (item in mPropertiesViewModelList) {
            if (item.tickChecked) {
                selected.add(item)
            }
        }
        return selected
    }


    fun unSelected() {
        val mListItemsTemp = mPropertiesViewModelList
        for (item in mListItemsTemp) {
            if (item.tickChecked) {
                item.tickChecked = false
                mPropertiesViewModelList.remove(item)
                EMPITYSELECTED=true
                ISEDITABLE = false
            }
        }
        notifyDataSetChanged()
    }


    private fun openImageDialog(propertiesModel: Properties) {
        try {
            val openImageDialog = Dialog(mContext)
            openImageDialog.setContentView(R.layout.dialog_open_image)
            val imageView =openImageDialog.findViewById<ImageView>(R.id.dialogeOpenImage_imageView)
            openImageDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            openImageDialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
            openImageDialog.show()
            openImageDialog.setCancelable(true)
                imageView.setImageBitmap(getImage(propertiesModel))

        } catch (er: java.lang.Exception) {
            Log.e("Error",checkNotNull(er.message))
        }
    }
}



class HistoryItemClick : Thread() {

    fun searchWeb(str: String?) {
        mContext.vibrate(30)
        val search = Intent(Intent.ACTION_WEB_SEARCH)
        search.putExtra(SearchManager.QUERY, str)
        mContext.startActivity(search)
    }


}

