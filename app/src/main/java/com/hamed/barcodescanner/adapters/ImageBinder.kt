package com.hamed.barcodescanner.adapters

import android.graphics.*
import android.widget.EditText
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer

@BindingAdapter("GenerateImage")
fun generateImage(mimageView: ImageView, bitmap: MutableLiveData<Bitmap>?) {
    if (bitmap!=null) {
        bitmap.observe(mimageView.context as LifecycleOwner, Observer<Bitmap> { bm: Bitmap? ->
            if (bm != null) {
                val imageRounded = Bitmap.createBitmap(bm.width, bm.height, bm.config)
                val canvas = Canvas(imageRounded)
                val mpaint = Paint()
                mpaint.isAntiAlias = true
                mpaint.shader = BitmapShader(bm, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)
                canvas.drawRoundRect(
                    RectF(0f, 0f, bm.width.toFloat(), bm.height.toFloat()),
                    5f,
                    5f,
                    mpaint
                ) // Round Image Corner 100 100 100 100
                mimageView.setImageBitmap(imageRounded)
            }
        })
    }
}






