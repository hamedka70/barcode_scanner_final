package com.hamed.barcodescanner.adapters

import android.view.MenuItem
import androidx.databinding.BindingAdapter
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.hamed.barcodescanner.R
import com.hamed.barcodescanner.view.GenerateFragment
import com.hamed.barcodescanner.view.HistoryFragment
import com.hamed.barcodescanner.view.ScannerActivity
import com.hamed.barcodescanner.view.ScannerFragment

@BindingAdapter("onNavigationItemSelected")
fun  setOnNavigationItemSelected(view: BottomNavigationView, listener: BottomNavigationView) {
    val activity= ScannerActivity()
    view.setOnNavigationItemSelectedListener { item: MenuItem ->
            when (item.itemId) {
                R.id.navigation_home -> {
                    activity.openFragment(ScannerFragment.newInstance())
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.navigation_list ->{
                    activity.openFragment(HistoryFragment.newInstance())
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.navigation_add ->{
                    activity.openFragment(GenerateFragment.newInstance())
                    return@setOnNavigationItemSelectedListener true
                }
            }
        return@setOnNavigationItemSelectedListener false
    }
}