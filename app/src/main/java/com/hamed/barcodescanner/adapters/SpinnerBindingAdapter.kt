package com.hamed.barcodescanner.adapters

import android.view.View
import android.widget.AdapterView
import android.widget.Spinner
import androidx.appcompat.widget.AppCompatSpinner
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData

@BindingAdapter("spinner_click")
fun listenSpinnerValue(spinner: Spinner,result:MutableLiveData<String>) {
    spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {

        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long ) {
            result.postValue( parent?.getItemAtPosition(position) as String)
        }
    }


}