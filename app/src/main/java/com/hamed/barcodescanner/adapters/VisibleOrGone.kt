package com.hamed.barcodescanner.adapters

import android.view.View
import androidx.databinding.BindingAdapter


@BindingAdapter("isVisible")
fun bindIsGone(view: View, isGone:Boolean) {

        view.visibility = if (isGone) {
            View.VISIBLE
        } else {
            View.GONE
        }
}