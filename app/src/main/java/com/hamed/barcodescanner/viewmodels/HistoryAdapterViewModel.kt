package com.hamed.barcodescanner.viewmodels

import androidx.lifecycle.ViewModel
import com.hamed.barcodescanner.data.Properties

class HistoryAdapterViewModel(properties: Properties) : ViewModel() {
    val mProperties =properties
    val vmID
        get()= mProperties.id
    val vmBarcode
        get() = mProperties.barcode
    val vmBarcodeFormat
        get() = mProperties.barcodeFormat
    val vmPropertiesName
        get() = mProperties.propertiesName
    val vmTime
        get() = mProperties.time
    val  vmImageAddress
        get() = mProperties.imageAddress
    val vmTickChecked
        get() = mProperties.tickChecked
}