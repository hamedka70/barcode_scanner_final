package com.hamed.barcodescanner.viewmodels

import android.app.Application
import android.graphics.Bitmap
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.zxing.MultiFormatWriter
import com.hamed.barcodescanner.Repository.PropertiesRepository
import com.hamed.barcodescanner.data.Properties
import com.hamed.barcodescanner.data.PropertiesDatabase
import com.hamed.barcodescanner.utilities.ShamsiDate
import com.hamed.barcodescanner.utilities.bitmap
import com.hamed.barcodescanner.utilities.getBarcodeFormat
import com.hamed.barcodescanner.utilities.saveImageSDCARD
import com.hamed.barcodescanner.view.GenerateFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*

class GenerateViewModel(application: Application) : AndroidViewModel(application) {
    var mImageAddress: String? = null
    var mPropertiesName: String? = null
    var mBarcodeLiveData = MutableLiveData<String>()
    var mBarcode: String
    lateinit var mTime: String
    var mBarcodeFormatLiveData = MutableLiveData<String>()
    var mBarcodeFormat: String? = null
    var bitmap = MutableLiveData<Bitmap>()
    val generateFragment=
        GenerateFragment()
    private var repository: PropertiesRepository


    init {
        mBarcode = ""
        mBarcodeFormatLiveData.value = ""
        mBarcodeLiveData.value = ""
        bitmap.value = null
        val propertiesDao = PropertiesDatabase.getPropertiesDatabase(application).propertiesDao()
        repository = PropertiesRepository(propertiesDao)
    }



    fun insert()= viewModelScope.launch(Dispatchers.IO) {
        val multiFormatWriter = MultiFormatWriter()
        mImageAddress = saveImageSDCARD(
            bitmap(mBarcode, getBarcodeFormat(mBarcodeFormat), multiFormatWriter),
            (repository.countProperties()+1).toString()
        )
        val currentDate: String = ShamsiDate.getCurrentShamsidate()
        val cc = Calendar.getInstance()
        val currentTime = String.format(
            "%02d:%02d:%02d"
            , cc[Calendar.HOUR_OF_DAY]
            , cc[Calendar.MINUTE], cc[Calendar.SECOND]
        )
        mTime = currentDate + "\n" + currentTime

        val properties = Properties(mImageAddress, mPropertiesName, mBarcode, mTime, mBarcodeFormat)
            repository.insert(properties)
    }
}