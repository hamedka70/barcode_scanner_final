package com.hamed.barcodescanner.viewmodels

import android.app.Activity
import android.util.Log
import android.view.MenuItem
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.hamed.barcodescanner.R
import com.hamed.barcodescanner.view.GenerateFragment
import com.hamed.barcodescanner.view.HistoryFragment
import com.hamed.barcodescanner.view.ScannerActivity
import com.hamed.barcodescanner.view.ScannerFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ScannerActivityViewModel : ViewModel(){
    var scannerActivity:ScannerActivity

    init {
        scannerActivity= ScannerActivity()
        //scannerActivity.openFragment(ScannerFragment.newInstance())
    }
    fun  setOnNavigationItemSelected(view: BottomNavigationView){
        view.setOnNavigationItemSelectedListener { item: MenuItem ->
            when (item.itemId) {
                R.id.navigation_home -> {
                    Log.e("home","Home")
                    scannerActivity.openFragment(ScannerFragment.newInstance())
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.navigation_list ->{
                    try {
                        scannerActivity.openFragment(HistoryFragment.newInstance())
                        Log.e("list","List")

                    }catch (e:Exception){
                        Log.e("er", checkNotNull(e.message))
                    }
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.navigation_add ->{
                    Log.e("add","ADD")

                    scannerActivity.openFragment(GenerateFragment.newInstance())
                    return@setOnNavigationItemSelectedListener true
                }
            }
            return@setOnNavigationItemSelectedListener false
        }
    }

}