package com.hamed.barcodescanner.viewmodels

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hamed.barcodescanner.Repository.PropertiesRepository
import com.hamed.barcodescanner.data.Properties
import com.hamed.barcodescanner.data.PropertiesDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ScannerViewModel(application: Application):AndroidViewModel (application){
    private val repository:PropertiesRepository

    init {
        val propertiesDao= PropertiesDatabase.getPropertiesDatabase(application).propertiesDao()
        repository= PropertiesRepository(propertiesDao)

    }

    fun insert (properties: Properties)= viewModelScope.launch(Dispatchers.IO){
        repository.insert(properties)
    }
}