package com.hamed.barcodescanner.viewmodels

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.lifecycle.*
import com.hamed.barcodescanner.Repository.PropertiesRepository
import com.hamed.barcodescanner.adapters.HistoryAdapter
import com.hamed.barcodescanner.data.Properties
import com.hamed.barcodescanner.data.PropertiesDatabase
import com.hamed.barcodescanner.utilities.deleteImageSdCard
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class HistoryViewModel(application: Application) :AndroidViewModel(application){
    var repository:PropertiesRepository
    lateinit var mAllProperties : LiveData<List<Properties>>
    var Properties =MutableLiveData<List<Properties>>()



    init {
        val propertiesDao= PropertiesDatabase.getPropertiesDatabase(application).propertiesDao()
        repository= PropertiesRepository(propertiesDao)
        try {
            mAllProperties = repository.allProperties
        }catch (e:Exception){
            Log.e("error",checkNotNull(e.message))
        }
    }


    fun returnPropertiesLiveDataList():MutableLiveData<List<Properties>>{
        return Properties
    }
    fun initProperties(properties: Properties){

    }

    fun delete (id:Int)= viewModelScope.launch(Dispatchers.IO){
        repository.delete(id)
    }


    fun deleteItem() {
        if (Properties.value==null){

        }
        else
        for (item in Properties.value as ArrayList) {
            if (item.tickChecked) {

                delete(item.id)
                deleteImageSdCard(item.imageAddress)
            }
        }
    }

    fun selectAll() {
        for (item in Properties.value as ArrayList) {
            if (!item.tickChecked) {
                item.tickChecked = true
            }
        }
    }
}