package com.hamed.barcodescanner.utilities

import android.content.Context
import android.graphics.*
import android.os.Build
import android.os.Environment
import android.os.VibrationEffect
import android.os.Vibrator
import android.preference.PreferenceManager
import android.util.Log
import android.widget.ImageView
import android.widget.Toast
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.hamed.barcodescanner.data.Properties
import com.journeyapps.barcodescanner.BarcodeEncoder
import java.io.File
import java.io.FileOutputStream
import java.util.*


fun Context.vibrate(duration: Long) {
    val vib = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        vib.vibrate(VibrationEffect.createOneShot(duration, VibrationEffect.DEFAULT_AMPLITUDE))
    } else {
        @Suppress("DEPRECATION")
        vib.vibrate(duration)
    }
}



fun saveImageSDCARD(bitmapImage: Bitmap?, imgname: String): String? {
    var mImageAddress =""
    try {
        val sdCardRoot = Environment.getExternalStorageDirectory().toString()
        val filename = "$imgname.jpg"
        val myDir = File("$sdCardRoot/Pictures/Iranian BS")
        val file = File(myDir, filename)
        val fileOutput = FileOutputStream(file)
        mImageAddress = "$sdCardRoot/Pictures/Iranian BS/$filename"
        bitmapImage?.compress(Bitmap.CompressFormat.JPEG, 100, fileOutput)
        fileOutput.close()
    } catch (e: Exception) {
        Log.e("Error ", checkNotNull(e.message))
    }
    return mImageAddress
}

fun getImage(properties: Properties): Bitmap? {
    try {
        val imgFile: File
        val path: String? = properties.imageAddress
        imgFile = File(path)
        return BitmapFactory.decodeFile(imgFile.absolutePath)
    }catch (e:Exception){
        Log.e("no file" , checkNotNull(e.message))
        return null
    }
}

fun randomColor(): Int {
    val rnd = Random()
    return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))
}

fun deleteImageSdCard(imgAddress: String?) {
    val file = File(imgAddress)
    file.delete()
}

fun generateImage(mimageView: ImageView, bitmap: Bitmap) {
    val imageRounded = Bitmap.createBitmap(bitmap.width, bitmap.height, bitmap.config)
    val canvas = Canvas(imageRounded)
    val mpaint = Paint()
    mpaint.isAntiAlias = true
    mpaint.shader = BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)
    canvas.drawRoundRect(
        RectF(0f, 0f, bitmap.width.toFloat(), bitmap.height.toFloat()),
        5f,
        5f,
        mpaint
    ) // Round Image Corner 100 100 100 100
    mimageView.setImageBitmap(imageRounded)
}

fun bitmap(
    result: String?,
    barcodeFormat: BarcodeFormat?,
    multiFormatWriter: MultiFormatWriter
): Bitmap? {
    return try {
        val bitMatrix = multiFormatWriter.encode(result, barcodeFormat, 600, 600)
        val barcodeEncoder = BarcodeEncoder()
        barcodeEncoder.createBitmap(bitMatrix)
    } catch (e: java.lang.Exception) {
        Log.e("Error", checkNotNull(e.message))
        null
    }
}


fun Context.toast(message: CharSequence) =
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()


fun getBarcodeFormat(bf: String?): BarcodeFormat? {
    when (bf) {
        "EAN_8" -> return BarcodeFormat.EAN_8
        "UPC_E" -> return BarcodeFormat.UPC_E
        "EAN_13" -> return BarcodeFormat.EAN_13
        "UPC_A" -> return BarcodeFormat.UPC_A
        "QR_CODE" -> return BarcodeFormat.QR_CODE
        "CODE_39" -> return BarcodeFormat.CODE_39
        "CODE_93" -> return BarcodeFormat.CODE_93
        "CODE_128" -> return BarcodeFormat.CODE_128
        "CODABAR" -> return BarcodeFormat.CODABAR
        "DATA_MATRIX" -> return BarcodeFormat.DATA_MATRIX
        "AZTEC" -> return BarcodeFormat.AZTEC
        "PDF_417" -> return BarcodeFormat.PDF_417
        "ITF" -> return BarcodeFormat.ITF
    }
    return null
}



fun initSettings(mContext: Context?) {
    lang.setLocale(mContext)
}